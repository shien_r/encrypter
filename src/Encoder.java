import java.util.Calendar;


public class Encoder {
	private final int BLOCK = 6;
	private final int INTERVAL = 4;
	private final String SPLIT_CHAR_CODE = "X";
	private final SymbolList symbols = new SymbolList();

	public String encode(String data, String sepalator) {
		String[] dataList = data.split(sepalator);
		sort(dataList);
		String[] tmp = encodeBlock(dataList);
		return combine(tmp);
	}
	
	public String encodeDate(Calendar calendar, int period) {
		String date=""; //TODO
		date += String.format("%02", period);
		String[] dataList = date.split("/");
		
		sort(dataList);
		String[] tmp = encodeBlock(dataList);
		return combine(tmp);
	}
	
	/* encodeBlock
	 * dataList: 各二桁のブロックに分けたデータを暗号化
	 * 
	 * 
	 */	
	private String[] encodeBlock(String[] dataList) {
		String[] tmp = new String[BLOCK];		
		for (int i=0; i < dataList.length; i++) {
			tmp[i] = changeToSymbol(dataList[i]);
		}
		return tmp;
	}
	
	/*　changeToSymbol
	 * twoDigitHex: 二桁の文字列の１６進数
	 * 直接値を暗号化されたものが表せないように、INTERVAL
	 * を加える
	 */	
	private String changeToSymbol(String twoDigitHex) {
		String result = "";
		for (int i=0; i < twoDigitHex.length(); i++) {
			String tmp = twoDigitHex.substring(i,i+1);
			int digit = Integer.parseInt(tmp, 16) + INTERVAL; // 16 進数で			
			digit %= 15;
			result += symbols.getSymbol(digit);			
		}
		return result;
	}
	
	/* 
	 * sort
	 * dataList: ブロック分割されたデータのリスト
	 * 
	 * 指定された順序で並び替え
	 * 1 2 3 4 5 6 と並んでいたとすれば、
	 * 1 4 2 5 3 6 と並ぶ
	 */	
	private void sort(String[] dataList) {		
		String[] tmpData = new String[dataList.length];		
		int length = dataList.length;
		for (int i=0,j=0; i<length; i+=2,j++) {						
				tmpData[i] = dataList[j];				
		}
		
		for (int i=1, j=length/2; i<length; i+=2, j++) {
			tmpData[i] = dataList[j];
		}		
		
		for(int i=0; i < length; i++) {
			dataList[i] = tmpData[i];
		}
	}
	
	/* combine 
	 * dataList: 暗号化された個々のブロックをもつ配列
	 * SPLIT_CHAR で結合する
	 * 暗号化の最後の段階
	 * 結合されたデータを返す
	 */
	private String combine(String[] dataList) {
		String combineData = dataList[0];
		for(int i=1; i < dataList.length; i++) {
			combineData += SPLIT_CHAR_CODE + dataList[i];
		}
		return combineData;
	}
	
}
