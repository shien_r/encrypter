
public class Decoder {
	private final int BLOCK = 6;
	private final int INTERVAL = 4;
	private final String SPLIT_CHAR_CODE = "X";
	private final String SPLIT_CHAR_MAC = ":";
	private final SymbolList symbols = new SymbolList();
	private String sepalator = SPLIT_CHAR_MAC;
	
	/* decode 
	 * code: 暗号化解除をしたいコード 
	 * _sepalator: 暗号化した後、分割に使いたい記号
	 * 
	 * return:
	 * 
	 */		
	public String decode(String code, String _sepalator) {
		sepalator = _sepalator;
		String codeList[] = code.split(SPLIT_CHAR_CODE);
		sort(codeList);		
		String[] tmp = decodeBlock(codeList);			
		return combine(tmp);
	}
	
	private String[] decodeBlock(String[] codeList)  {
		String[] tmp = new String[BLOCK];
		int length = codeList.length;
		for (int i=0; i < length; i++) {
			tmp[i] = changeToNumber(codeList[i]);			
		}
		return tmp;
	}
	
	private String changeToNumber(String code) {
		String result = "";
		for (int i=0; i < code.length(); i++) {
			String tmp = code.substring(i,i+1);
			int digit = symbols.getNumber(tmp);
			digit = digit - INTERVAL;
			if (digit < 0) {
				digit = 15 + digit;
			}
			result += Integer.toHexString(digit);
		}
		return result;
	}

	private void sort(String[] dataList) {
		String[] tmpData = new String[dataList.length];
		int length = dataList.length;
		for (int i=0, j=0; i < length; i+=2,j++) {
			tmpData[j] = dataList[i];
		}
		
		for (int i=1, j=length/2; i<length; i+=2, j++) {
			tmpData[j] = dataList[i];
		}
		
		for(int i=0; i < length; i++) {
			dataList[i] = tmpData[i];
		}
	}
	
	private String combine(String[] dataList) {
		String combineData = dataList[0];
		for(int i=1; i < dataList.length; i++) {
			combineData += sepalator + dataList[i];
		}
		return combineData;
	}
}
