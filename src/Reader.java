import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


public class Reader {
	private BufferedReader bufferedReader;
	
	Reader() {
		FileReader fileReader;
		try {
			fileReader = new FileReader("certificate.txt");
			bufferedReader = new BufferedReader(fileReader);			
		} catch (FileNotFoundException e) {
			System.out.println("File not found.");
		}
	}	
	
	public String read() {
		String readData = "";
		try {
			readData = bufferedReader.readLine();
		} catch (IOException e) {
			System.out.println("Can't read file.");
			//e.printStackTrace();
		}
		return readData;		
	}
	
	public void close() {
		try {
			bufferedReader.close();
		} catch (IOException e) {
			System.out.println("Can't close.");
			e.printStackTrace();
		}
	}
}
