import java.util.ArrayList;


public class SymbolList {
	ArrayList<String> symbolList = new ArrayList<String>();
	
	SymbolList() {
		symbolList.add("^"); // 0
		symbolList.add("#");
		symbolList.add("*");
		symbolList.add("-");
		symbolList.add("+");
		symbolList.add("[");
		symbolList.add("%");
		symbolList.add("&");
		symbolList.add("|");
		symbolList.add("@");
		symbolList.add(":");
		symbolList.add("$");
		symbolList.add("]");
		symbolList.add("_");
		symbolList.add("{");
		symbolList.add("<"); // 15	
	}
	
	public String getSymbol(int symbolNumber) {
		return symbolList.get(symbolNumber);
	}
	
	public int getNumber(String symbol) {
		return symbolList.indexOf(symbol);
	}
}
