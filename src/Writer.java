import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;


public class Writer {	
	private PrintWriter printWriter;

	Writer(String fileName) {
		File file = new File(fileName);
		try {			
			printWriter = new PrintWriter(new BufferedWriter(new FileWriter(file)));
		} catch (IOException e) {
			System.out.println("Can't write to file.");			
			e.printStackTrace();
		}
	}
	
	public void write(String writeData) {
		printWriter.println(writeData);				
	}
	
	public void close() {
		printWriter.close();
	}
}
